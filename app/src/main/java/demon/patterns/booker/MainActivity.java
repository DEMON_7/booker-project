package demon.patterns.booker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("JavascriptInterface")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DBHelper.getInstance(this); // will call initializing
        sThis = this;

        setContentView(R.layout.activity_main);

        WebView webView = (WebView) findViewById(R.id.web_view);
        JSInterface jsInterface = new JSInterface(this);
        MainActivity.configureWebView(webView, jsInterface);

        WebController.init(this, webView );
        WebController.regHtml("file:///android_asset/html/start-page.html",
                WebController.Pages.MainPage);
        WebController.regHtml("file:///android_asset/html/booking-edit.html",
                WebController.Pages.BookingEdit);

        WebController.regHtml("file:///android_asset/html/calendar.html",
                WebController.Pages.Calendar);

        WebController.regHtml("file:///android_asset/html/house-edit.html",
                WebController.Pages.HouseEdit);
        try {
            // WebController.showHtml(WebController.Pages.MainPage);
            WebController.showHtml(WebController.Pages.MainPage);
        } catch (FileNotFoundException e) {
            System.out.println("main activity can't open page");
        }

        sContext = this;
    }

    @SuppressLint("JavascriptInterface")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static void configureWebView(WebView webView, JSInterface jsInterfase) {
        webView.setInitialScale(1);
        webView.setLongClickable(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.addJavascriptInterface(jsInterfase, "JAVA");
    }

    @Override
    public void onBackPressed() {
        if (WebController.currentPage() == WebController.Pages.MainPage)
          onDestroy();
        else {
            try {
                WebController.showHtml(WebController.Pages.MainPage);
            } catch (FileNotFoundException e) {
                System.out.println("Main activity: main page not found");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) { // request calling
            JSInterface.repeatCall();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("DB state saving -------");
        System.runFinalizersOnExit(true);
        System.exit(0);
    }

    private static Context sContext;
    private static MainActivity sThis = null;
    public static Context getContext() {
        return sContext;
    }

    public static MainActivity getThis() {
        return sThis;
    }
}
