package demon.patterns.booker;

public class Log {
    private static boolean mEnabled = true;
    public static void debug(Object sender, String info) {
        if (mEnabled)
            // System.out.println(sender + " : " + info);
            android.util.Log.d(sender.toString(), info);
    }
}
