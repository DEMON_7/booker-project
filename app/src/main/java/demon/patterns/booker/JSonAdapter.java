package demon.patterns.booker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 *  Схема хранимых объектов
 *
 *  для всех объектов(for every data object):
 *      String id
 *
 *  booking(bookings_table):
 *      String house
 *      String desc
 *      String (GregoriandCalendar.toString()) date_start
 *      String (GrerCalendar.toString()) date_end
 *      String tel_number
 *
 *  house(house_table)
 *    String desc
 */

public class JSonAdapter {
    public static JSONObject packBooking(String id, String house, String desc,
                                         String dtStart, String dtEnd, String telNumber) {
        JSONObject json = new JSONObject();
        try {
            json.put("id", id);
            json.put("house", house);
            json.put("desc", desc);
            json.put("date_start", dtStart);
            json.put("date_end", dtEnd);
            json.put("tel_number", telNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    public static JSONObject packHouse(String id, String desc) {
        JSONObject json = new JSONObject();
        try {
            json.put("id", id);
            json.put("desc", desc);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    // // data: id[0] % house[1] % desc[2] % dateStart[3] % dateEnd[4] % tel_number[5] % id2[6] % desc2%....
    public static String bookingsToString(HashMap<String, JSONObject> bookings) {
        StringBuilder data = new StringBuilder();
        for (String k : bookings.keySet()) {
            JSONObject b = bookings.get(k);
            try {
                data.append(b.getString("id") + "|");
                data.append(b.getString("house") + "|");
                data.append(b.getString("desc") + "|");
                data.append(b.getString("date_start") + "|");
                data.append(b.getString("date_end") + "|");
                if (b.has("tel_number"))
                  data.append(b.getString("tel_number") + "|");
                else
                    data.append("-|");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (data.length() != 0)
            data.deleteCharAt(data.length()-1); // removing last % in the end of string
        return data.toString();
    }
}
