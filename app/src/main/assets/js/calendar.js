var lastDayInMonth
var rangesDescs = []
var currentSelectedRange
var rangeColors = ['#8bc34a', '#673ab7', '#009688', '#ff9800', '#795548', '#607d8b']

function onDateClicked(event) {  
      let dt = Number.parseInt(event.target.innerHTML)
      console.log('Clicked date: ' + dt)
      for (range of rangesDescs) 
       if (range != undefined)
        if (range.start <= dt && dt <= range.end) {
          let currentBooking = document.getElementById("current_booking")
          let currentDesc = document.getElementById("booking_desc")
          let currentStart = document.getElementById("booking_start")
          let currentEnd = document.getElementById("booking_end")
          let currentTel = document.getElementById("tel")
          let callIcon = document.getElementById("call_icon")

          currentBooking.style.display = 'block'
          currentDesc.innerHTML = range.description
          currentStart.innerHTML = range.start
          currentEnd.innerHTML = range.end    
          currentTel.innerHTML = range.tel
          callIcon.id = range.tel

          if (currentSelectedRange) {
            let start = currentSelectedRange.start
            let end = currentSelectedRange.end
            markLockPeriod(start, end, 'red')              
          }        

          currentSelectedRange = {
            start: Number.parseInt(range.start),
            end: Number.parseInt(range.end)
          }

          markLockPeriod(range.start, range.end, 'blue')
          return 1
        }                
}

function fillMonth(firstWeekDay, lasDate) {
  while (calendar__content.firstChild) 
      calendar__content.removeChild(calendar__content.firstChild);  

  lastDayInMonth = lasDate

  let days = ["П", "В", "С", "Ч", "П", "C", "В"]
  for (let i = 0; i < days.length; ++i) {
    let newDate = document.createElement('div');
    newDate.setAttribute('class', 'calendar__day');
    newDate.innerHTML = days[i];
    calendar__content.appendChild(newDate);
  }

  for (let i = 1; i < firstWeekDay; ++i) {
    let newDate = document.createElement('div');
    newDate.setAttribute('class', 'calendar__number');
    newDate.setAttribute('id', 'dtc' + i)
    newDate.innerHTML = ' '
    calendar__content.appendChild(newDate);
  }

  for (i = 1; i <= lasDate; ++i) {
    let newDate = document.createElement('div');
    newDate.setAttribute('class', 'calendar__number');
    newDate.setAttribute('id', 'dt' + i)
    newDate.innerHTML = i
    calendar__content.appendChild(newDate)     
    newDate.addEventListener('click', onDateClicked, true)
  }
}

function setDate(month, date, day) {
  document.getElementById("month_header").innerHTML = month;
}

function markMonth(dRanges, descriptions, telephones) {
    let ranges = dRanges.split("|")
    let descs = descriptions.split('|')
    let telNums = telephones.split('|')
    for (let i = 0; i < ranges.length; i += 2) {
      markLockPeriod(ranges[i], ranges[i+1], 'red')
      rangesDescs[i] = {
        description: descs[i],
        start: Number.parseInt(ranges[i]),
        end: Number.parseInt(ranges[i+1]),
        tel: telNums[i]
      }
    }      

    console.log('Ranges array is created: ' + rangesDescs)
}

function markLockPeriod(dateStart, dateEnd, color) {
  if (color != 'blue') {
    let min = 0
    let max = rangeColors.length - 1
    let randIndex = Math.floor(Math.random() * (max - min)) + min;
    color = rangeColors[randIndex]
  }

  for (let i = dateStart; i <= dateEnd; ++i) 
    document.getElementById('dt' + String(i)).style.background = color;   
}

function addHouse(id, desc) {
  let upperToolbar = document.getElementById("toolbar-upper")
  let item = createUpperBarLi(id, desc, 'noImage')   
  item.setAttribute('onclick', 'onClickHouse(this)')
  upperToolbar.appendChild(item)
}

function onClickHouse(obj) {
  let houseId = obj.dataset.id
  currentSelectedRange = undefined
  for (let i = 1; i <= lastDayInMonth; ++i)
    document.getElementById('dt' + i).style.background = '#191919'
    
  JAVA.onCalendarHouseClick(houseId)
}