function createBookingLi(id, house, desc, dateStart, dateEnd, telNum) {
    let booking_li = document.createElement('li')
    booking_li.setAttribute('class', 'booking')
    booking_li.setAttribute('data-house', house)
  
    let booking_info = document.createElement('div')
    booking_info.setAttribute('class', 'booking-info')
  
    let booking_desc = document.createElement('div')
    booking_desc.setAttribute('class', 'booking-item')
    booking_desc.innerHTML = desc
  
    let booking_dstart = document.createElement('div')
    booking_dstart.setAttribute('class', 'booking-item')
    booking_dstart.innerHTML = dateStart
  
    let booking_dend = document.createElement('div')
    booking_dend.setAttribute('class', 'booking-item')
    booking_dend.innerHTML = dateEnd
  
    let booking_tel = document.createElement('div')
    booking_tel.setAttribute('class', 'booking-item')
    booking_tel.innerHTML = telNum
  
    let booking_edit_panel = document.createElement('div')
    booking_edit_panel.setAttribute('class', 'booking-edit-panel')

    let booking_img = document.createElement('img')
    booking_img.style.backgroundColor = "#191919"
    booking_img.setAttribute('src', '../icons/delete.png')
    booking_img.setAttribute('class', 'booking-img')
    booking_img.setAttribute('id', id)
    booking_img.setAttribute('onclick', 'JAVA.onBookingRemoveClicked(id)')
  
    let booking_img_call = document.createElement('img')
    booking_img_call.style.backgroundColor = "#191919"
    booking_img_call.setAttribute('src', '../icons/call.png')
    booking_img_call.setAttribute('class', 'booking-img')
    booking_img_call.setAttribute('id', telNum)
    booking_img_call.setAttribute('onclick', 'JAVA.callBooking(id)')
  
    // let booking_img_edit = document.createElement('img')
    // booking_img_edit.style.backgroundColor = "#191919"
    // booking_img_edit.setAttribute('src', '../icons/edit.png')
    // booking_img_edit.setAttribute('class', 'booking-img')
    // booking_img_edit.setAttribute('id', id)  
  
    booking_li.appendChild(booking_info)
    booking_li.appendChild(booking_edit_panel)

    booking_edit_panel.append(booking_img)
    booking_edit_panel.appendChild(booking_img_call)
  
    booking_info.appendChild(booking_desc)
    booking_info.appendChild(document.createElement('br'))
    booking_info.appendChild(booking_dstart)
    booking_info.appendChild(booking_dend)
    booking_info.appendChild(booking_tel)
  
    return booking_li
  }

  function createUpperBarLi(id, desc, options = 'none') {
    let item = document.createElement('li')
    let itemImg = document.createElement('img')
    let itemTxt = document.createElement('div')
  
    item.setAttribute('class', "upper-bar-item")
    item.setAttribute('data-id', id)
    item.setAttribute('onclick', 'onClickHouse(this)')
    
    itemTxt.setAttribute('class', 'upper-bar-item-text')
    itemTxt.innerHTML = desc
    
    item.appendChild(itemTxt)
  
    if (options != 'noImage') {
        itemImg.setAttribute('class', 'upper-bar-img')
        itemImg.setAttribute('src', '../icons/delete.png')
        itemImg.setAttribute('id', id)
        itemImg.setAttribute('onclick', 'JAVA.onRemoveHouseClicked(id)')
        item.appendChild(itemImg)
    }
        
    return item
  }