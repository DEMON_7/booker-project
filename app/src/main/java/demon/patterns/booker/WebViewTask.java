package demon.patterns.booker;

import android.webkit.WebView;
import org.json.JSONException;
import org.json.JSONObject;

public class WebViewTask implements Runnable {
    WebController.Pages mPageId;
    WebView mView;
    Tasks mTask;
    JSONObject mData;
    String mStringData;

    public WebViewTask(WebView view, Tasks task) {
        mView = view;
        mTask = task;
    }

    public void setData(JSONObject json) {
        mData = json;
    }

    public void setData(String data) {
        mStringData = data;
    }

    public void setHtmlId(WebController.Pages id) {
        mPageId = id;
    }

    enum Tasks {
        SwitchHTML,
        AddBooking,
        AddBookings,
        AddHouseToUpToolbar,
        DeleteBooking,
        DeleteHouse,
        InitMonth,
        MarkMonth,
        AddHouseToSelect
    }

    @Override
    public void run() {
        switch (mTask) {
            case SwitchHTML:
                mView.loadUrl(WebController.getPage(mPageId));
                break;
            case AddBooking:
                try {
                    String id = mData.getString("id");
                    String house = mData.getString("house");
                    String desc = mData.getString("desc");
                    String dateStart = mData.getString("date_start");
                    String dateEnd = mData.getString("date_end");
                    String telNum = mData.getString("tel_number");

                    Log.debug(this, "calling js booking addition " + mData);
                    mView.loadUrl("javascript:addBooking('" + id + "','"
                            + house + "','" + desc + "','" + dateStart + "','"
                            + dateEnd + "','" + telNum + "');");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case AddHouseToSelect:
                try {
                    String id = mData.getString("id");
                    String desc = mData.getString("desc");

                    Log.debug(this, "Adding house to select " + id + " " + desc);
                    mView.loadUrl("javascript:addHouse('" + id + "','" + desc + "')");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case InitMonth:
                try {
                    String month = mData.getString("month");
                    String day = mData.getString("day");
                    String date = mData.getString("date");
                    String firstMonday = mData.getString("first_monday");
                    String nDays = mData.getString("number_days");

                    mView.loadUrl("javascript:fillMonth('" + firstMonday + "','" + nDays + "')");
                    mView.loadUrl("javascript:setDate('" + month + "','" + date + "','" + day + "')");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case AddBookings:
                Log.debug(this, "calling js bookings addition " + mStringData);
                mView.loadUrl("javascript:addBookings('" + mStringData + "')");
                break;

            case DeleteBooking:
                try {
                    String id = mData.getString("id");
                    Log.debug(this, "Deleting booking with id " + id);
                    mView.loadUrl("javascript:removeBooking(" + id + ")");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case DeleteHouse:
                try {
                    String id = mData.getString("id");
                    mView.loadUrl("javascript:removeHouse(" + id + ")");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case AddHouseToUpToolbar:
                try {
                    String id = mData.getString("id");
                    String desc = mData.getString("desc");
                    mView.loadUrl("javascript:addHouse('" + id + "','" + desc + "')");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case MarkMonth:
                Log.debug(this, "Mark month with: " + mData.toString());
                try {
                    String ranges = mData.getString("ranges");
                    String descs = mData.getString("descriptions");
                    String tels = mData.getString("telephones");
                    mView.loadUrl("javascript:markMonth('" + ranges + "','" + descs + "','" + tels + "')");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}