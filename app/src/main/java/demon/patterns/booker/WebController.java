package demon.patterns.booker;

import android.content.Context;
import android.webkit.WebView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class WebController {
    public static void init(Context c, WebView view) {
        sContext = c;
        sView = view;
    }

    public static void htmlLoaded() {
        Log.debug(sView, "Html loaded current html: " + currentPage());

        if (currentPage() ==  Pages.MainPage) {
            String bookings = JSonAdapter.bookingsToString(DBHelper.getTable(DBHelper.BOOKINGS_TABLE));
            if (bookings.length() != 0)
              addBookings(bookings);

            HashMap<String, JSONObject> houses = DBHelper.getTable(DBHelper.HOUSE_TABLE);
            for (String k : houses.keySet()) {
                WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.AddHouseToUpToolbar);
                task.setData(houses.get(k));
                sView.post(task);
            }
        }

        if (currentPage() == Pages.Calendar) {
            HashMap<String, JSONObject> houses = DBHelper.getTable(DBHelper.HOUSE_TABLE);
            for (String k : houses.keySet()) {
                WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.AddHouseToUpToolbar);
                task.setData(houses.get(k));
                sView.post(task);
            }

           uploadMonth(sCurrentCalendar);
        }

        if (currentPage() == Pages.BookingEdit) {
            HashMap<String, JSONObject> houses = DBHelper.getTable(DBHelper.HOUSE_TABLE);
            for (String k : houses.keySet()) {
                WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.AddHouseToSelect);
                task.setData(houses.get(k));
                sView.post(task);
            }
        }

       for (WebViewTask t : sTasks) {
           sView.post(t);
       }
       sTasks.clear();
    }

    public static void backMonth() {
        int currentMonth = sCurrentCalendar.get(Calendar.MONTH);
        int currentYear = sCurrentCalendar.get(Calendar.YEAR);

        if (currentMonth == 0) {
            --currentYear;
            sCurrentCalendar.set(Calendar.YEAR, currentYear);
            sCurrentCalendar.set(Calendar.MONTH, 0);
        }
        else {
            --currentMonth;
            sCurrentCalendar.set(Calendar.MONTH, currentMonth);
        }

        uploadMonth(sCurrentCalendar);
    }

    public static void nextMonth() {
        int currentMonth = sCurrentCalendar.get(Calendar.MONTH);
        int currentYear = sCurrentCalendar.get(Calendar.YEAR);

        if (currentMonth == 11) {
            ++currentYear;
            sCurrentCalendar.set(Calendar.YEAR, currentYear);
            sCurrentCalendar.set(Calendar.MONTH, 0);
        }
        else {
            ++currentMonth;
            sCurrentCalendar.set(Calendar.MONTH, currentMonth);
        }

        uploadMonth(sCurrentCalendar);
    }

    public static void regHtml(String fileName, Pages id) {
        sBuff.put(id, fileName);
    }

    public static void showHtml(Pages id) throws FileNotFoundException {
        if (!sBuff.containsKey(id))
            throw new FileNotFoundException("Html page not found");

        sCurrentPage = id;
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.SwitchHTML);
        task.setHtmlId(id);
        sView.post(task);
    }

    public static void addBooking(JSONObject data) {
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.AddBooking);
        task.setData(data);
        addDelayedTask(task);
    }

    public static void addBookings(String data) {
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.AddBookings);
        task.setData(data);
        sTasks.add(task);
    }

    public static void markMonth(String ranges, String descsForRanges, String telsForRanges) {
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.MarkMonth);
        JSONObject data = new JSONObject();
        try {
            data.put("ranges", ranges);
            data.put("descriptions", descsForRanges);
            data.put("telephones", telsForRanges);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        task.setData(data);
        sView.post(task);
    }

    public static void addHouse(JSONObject data) {
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.AddHouseToUpToolbar);
        task.setData(data);
        sTasks.add(task);
    }

    public static void deleteBooking(JSONObject data) {
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.DeleteBooking);
        task.setData(data);
        sView.post(task);
    }

    public static void deleteHouse(JSONObject data) {
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.DeleteHouse);
        task.setData(data);
        sView.post(task);
    }

    public static Pages strToId(String htmlName) {
        switch (htmlName) {
            case "main": return Pages.MainPage;
            case "booking-edit": return Pages.BookingEdit;
            case "calendar": return Pages.Calendar;
            case "house-edit": return Pages.HouseEdit;
            default: return Pages.MainPage;
        }
    }

    public static Pages currentPage() {
        return sCurrentPage;
    }

    public static String getPage(Pages p) {
        return sBuff.get(p);
    }

    public static int currentMonth() { return sCurrentCalendar.get(Calendar.MONTH); }

    private static void addDelayedTask(WebViewTask t) {
        sTasks.add(t);
    }

    private static void uploadMonth(GregorianCalendar c) {
        sCurrentCalendar = c;
        JSONObject json = new JSONObject();
        try {
            String numberDays = String.valueOf(c.getActualMaximum(
                    GregorianCalendar.DAY_OF_MONTH));
            String date = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

            json.put("number_days", numberDays);
            json.put("month", monthNumberToString(
                    c.get(Calendar.MONTH) + 1
            ));
            json.put("date", date);
            json.put("day", dayNumberToStr(
                    c.get(Calendar.DAY_OF_WEEK)
            ));

            GregorianCalendar tempC = (GregorianCalendar) sCurrentCalendar.clone();
            tempC.set(Calendar.DAY_OF_MONTH, 1);
            int weekDay = tempC.get(Calendar.DAY_OF_WEEK) - 1;

            json.put("first_monday", String.valueOf(weekDay));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.debug(sView, "Initializing of month with data: " + json);
        WebViewTask task = new WebViewTask(sView, WebViewTask.Tasks.InitMonth);
        task.setData(json);
        sView.post(task);
    }

    private static String monthNumberToString(int mNumber) {
        switch (mNumber) {
            case 1: return "Январь";
            case 2: return "Февраль";
            case 3: return "Март";
            case 4: return "Апрель";
            case 5: return "Май";
            case 6: return "Июнь";
            case 7: return "Июль";
            case 8: return "Август";
            case 9: return "Сентябрь";
            case 10: return "Октябрь";
            case 11: return "Ноябрь";
            case 12: return "Декабрь";
            default: return "";
        }
    }

    private static String dayNumberToStr(int dNumer) {
        switch (dNumer) {
            case 1: return "Понедельник";
            case 2: return "Вторник";
            case 3: return "Среда";
            case 4: return "Четверг";
            case 5: return "Пятница";
            case 6: return "Суббота";
            case 7: return "Воскресенье";
            default: return "";
        }
    }

    public enum Pages {
        MainPage,
        BookingEdit,
        HouseEdit,
        Calendar
    }

    private static Context sContext;
    private static WebView sView;
    private static HashMap<Pages, String> sBuff = new HashMap<>();
    private static Pages sCurrentPage = Pages.MainPage;
    private static ArrayDeque<WebViewTask> sTasks = new ArrayDeque<>();
    private static GregorianCalendar sCurrentCalendar = new GregorianCalendar();
}
