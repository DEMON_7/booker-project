package demon.patterns.booker;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.StringTokenizer;

import static android.support.v4.content.ContextCompat.startActivity;

public class JSInterface {

    public JSInterface(Context c) {
        mContext = c;
        sInstance = this;
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void debugLog(String text) {
        Log.debug(this, "JavaScript debug: " + text);
    }

    @JavascriptInterface
    public void onClickNextMonth() {
        WebController.nextMonth();
    }

    @JavascriptInterface
    public void onClickBackMonth() {
        WebController.backMonth();
    }

    @JavascriptInterface
    public void showPage(String pName) {
        try {
            WebController.showHtml(WebController.strToId(pName));
            System.out.println("jsINtrface page changed to " + pName);
        } catch (FileNotFoundException e) {
            System.out.println("jsInterface can't open page");
        }
    }

    @JavascriptInterface
    public void callBooking(String telNumber) {
        Log.debug("JSInterface", "Calling booking: " + telNumber);
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telNumber));

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.getThis(), new String[]{Manifest.permission.CALL_PHONE}, 1);
            JSInterface.sTelNumberWhilePermissionReq = telNumber;
        } else {
            mContext.startActivity(intent);
        }
    }

    // Repeat call after permission requiring
    public static void repeatCall() {
        sInstance.callBooking(sTelNumberWhilePermissionReq);
    }

    @JavascriptInterface
    public void onSaveHouseClicked(String hName) {
        Log.debug(this, "Saving new house: " + hName);
        String newId = DBHelper.generateId();
        JSONObject json = JSonAdapter.packHouse(newId, hName);
        try {
            WebController.showHtml(WebController.Pages.MainPage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        DBHelper.insert(DBHelper.HOUSE_TABLE, json);
    }

    @JavascriptInterface
    public void onCalendarHouseClick(String houseId) {
        Log.debug(this, "House clicked from calendar: " + houseId);

        HashMap<String, JSONObject> bookings = DBHelper.getTable(DBHelper.BOOKINGS_TABLE);
        StringBuilder ranges = new StringBuilder();
        StringBuilder descsForRanges = new StringBuilder();
        StringBuilder telsForRanges = new StringBuilder();
        int currentMonth =  WebController.currentMonth() + 1;

        for (String k : bookings.keySet()) {
            JSONObject json = bookings.get(k);
            try {
                String dateStart = json.getString("date_start");
                String dateEnd = json.getString("date_end");
                String hId = json.getString("house");
                String desc = json.getString("desc");
                String tel;
                if (json.has("tel_number"))
                   tel = json.getString("tel_number");
                else
                    tel = "-";

                StringTokenizer tokens = new StringTokenizer(dateStart, "-");
                tokens.nextToken(); // skip year
                int monthStart = Integer.valueOf(tokens.nextToken());
                int dayStart = Integer.valueOf(tokens.nextToken());

                tokens = new StringTokenizer(dateEnd, "-");
                tokens.nextToken(); // skip year
                int monthEnd = Integer.valueOf(tokens.nextToken());
                int dayEnd = Integer.valueOf(tokens.nextToken());

                if (houseId.equals(hId))
                // Весь период бронирования входит в текущий месяц
                if (monthStart == currentMonth && monthEnd == currentMonth) {
                    ranges.append(dayStart + "|");
                    ranges.append(dayEnd + "|");
                }
                else
                // Только конец периода бронирования входит в текущий месяц
                if (monthStart != currentMonth && monthEnd == currentMonth) {
                    ranges.append("0|");
                    ranges.append(dayEnd + "|");
                }
                else
                // Только начало периода бронирования входит в текущий месяц
                if (monthStart == currentMonth && monthEnd != currentMonth) {
                    ranges.append(dayStart + "|");
                    ranges.append("0|");
                }

                descsForRanges.append(desc).append("|");
                telsForRanges.append(tel).append("|");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        WebController.markMonth(ranges.toString(), descsForRanges.toString(), telsForRanges.toString());
    }

    @JavascriptInterface
    public void onRemoveHouseClicked(String id) {
        Log.debug(this, "remove house clicked with id " + id);
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());
        ad.setTitle("Удаление");  // заголовок
        ad.setMessage("Удалить объект ?"); // сообщение

        ad.setPositiveButton("Удалить", (DialogInterface dialog, int arg1) -> {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("id", id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    WebController.deleteHouse(json);
                    DBHelper.remove(DBHelper.HOUSE_TABLE, id);
                }
        );


        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });

        AlertDialog alert = ad.create();
        alert.show();
    }

    @JavascriptInterface
    public void onBookingRemoveClicked(final String id) {
        Log.debug(this, "remove booking clicked with id " + id);
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());
        ad.setTitle("Удаление");  // заголовок
        ad.setMessage("Удалить бронирование ?"); // сообщение

        ad.setPositiveButton("Удалить", (DialogInterface dialog, int arg1) -> {
                JSONObject json = new JSONObject();
            try {
                json.put("id", id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            DBHelper.remove(DBHelper.BOOKINGS_TABLE, id);
            WebController.deleteBooking(json);
            }
        );


        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });

        AlertDialog alert = ad.create();
        alert.show();
    }


    @JavascriptInterface
    public void onLoad() {
        Log.debug(this, "page loaded: " + WebController.currentPage());
        WebController.htmlLoaded();
    }

    @JavascriptInterface
    public void onSaveBookingClick(String house, String desc, String dt1, String dt2, String tel) {
        Log.debug(this ,"booking saving " + house + " " + desc + " " + dt1 + " " + dt2 + " " + tel);
        String newId = DBHelper.generateId();
        JSONObject json = JSonAdapter.packBooking(newId, house, desc, dt1, dt2, tel);
        DBHelper.insert(DBHelper.BOOKINGS_TABLE, json);
        try {
            WebController.showHtml(WebController.Pages.MainPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Context mContext;
    private static String sTelNumberWhilePermissionReq;
    private static JSInterface sInstance;
}
