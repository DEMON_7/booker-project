// // data: id[0] % house[1] % desc[2] % dateStart[3] % dateEnd[4] & tel_num[5] % id2[6] %desc2%....
function addBookings(data) {
    JAVA.debugLog("House data: " + data)
    let bookings = data.split("|")
    for (let i = 0; i < bookings.length; i += 6) {
      let id = bookings[i]
      let house = bookings[i + 1]
      let desc = bookings[i + 2]
      let dateStart = bookings[i + 3]
      let dateEnd = bookings[i + 4]
      let telNum = bookings[i + 5]
      JAVA.debugLog("House addition: " + id + " " + house + " " + desc + " " + dateStart + " " + dateEnd + " " + telNum)
      addBooking(id, house, desc, dateStart, dateEnd, telNum)
    }
  }
  
  function addBooking(id, house, desc, dateStart, dateEnd, telNum) {
    let bookings_list = document.getElementById("list_bookings")
    bookings_list.appendChild(createBookingLi(id, house, desc, dateStart, dateEnd, telNum))
  }
  
  function clearBookings() {
    while (list_bookings.length != 0) {
      list_bookings.removeChild(list_bookings.firstChild)
    }
  }
  
  function clearHouse() {
    let upperToolbar = document.getElementById("toolbar-upper")
    while (upperToolbar.length != 0) {
      upperToolbar.removeChild(list_bookings.firstChild)
    }
  }
  
  function addHouse(id, desc) {
    let upperToolbar = document.getElementById("toolbar-upper") 
    upperToolbar.appendChild(createUpperBarLi(id, desc))
  }
  
  function removeHouse(id) {
        let house = document.getElementById(id)
        house.parentNode.remove()
  
        let bookings = document.getElementById("list_bookings")
        for (let i = 0; i < bookings.childNodes.length; ++i) {
            let b = bookings.childNodes[i]
            if (b.nodeName != 'LI')
              continue;
  
            if (b.dataset.house == id) {
              b.remove()
                --i
            }
        }
  }
  
  function onClickHouse(obj) {
    let houseId = obj.dataset.id
    let bookings = document.getElementById("list_bookings")
    for (let i = 0; i < bookings.childNodes.length; ++i) {
      let b = bookings.childNodes[i]
      if (b.nodeName != 'LI')
        continue;
  
      if (b.dataset.house != houseId)
        b.style.display = 'none'    
      else
        b.style.display = 'block'    
    }
  }
  
  function removeBooking(id) {
      let booking = document.getElementById(id)
      booking.parentNode.parentNode.remove()
  }