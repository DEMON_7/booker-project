package demon.patterns.booker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    public static DBHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DBHelper(context);
            DBHelper.init();
        }

        return mInstance;
    }

    public static String generateId() {
        getInstance(MainActivity.getContext());

        if (idCounter == -1) {
            JSONObject js = get(TABLE_SERVICE, DB_ID_COUNTER);
            if (js == null)
                return "0";

            try {
                idCounter = Integer.parseInt(js.get(DB_ID_COUNTER).toString());
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ERROR ID COUNTER IS NOT LOADED");
                return "0";
            }
        }

        ++idCounter;

        JSONObject js = new JSONObject();
        try {
            js.put(DBHelper.DB_ID_COUNTER, idCounter);
            js.put(DBHelper.JSON_ID, DBHelper.DB_ID_COUNTER);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        DBHelper.update(DBHelper.TABLE_SERVICE, js);

        return idCounter + "";
    }

    public static void insert(String tableName, JSONObject json) {
        if (!json.has(JSON_ID))
            throw new RuntimeException("Json object do not contains id");

        SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            if (mCash.containsKey(tableName))
                mCash.get(tableName).put(json.getString("id"), json);
            values.put(JSON_ID, json.get(JSON_ID).toString());
            values.put(JSON_FIELD, json.toString());
        } catch (JSONException e) {
            System.out.println("Some value in JSON is empty");
        }

        db.insert(tableName, null, values);
    }

    public static void update(String tableName, JSONObject json) {
        if (!json.has(JSON_ID))
            throw new RuntimeException("Json object do not contains id");

        SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();
        ContentValues values = new ContentValues();
        String id = null;
        try {
            values.put(JSON_ID, json.get(JSON_ID).toString());
            values.put(JSON_FIELD, json.toString());
            id = json.get(JSON_ID).toString();
        } catch (JSONException e) {
            System.out.println("Some value in JSON is empty");
            return;
        }

        db.update(tableName, values, "id = ?", new String[]{id});
    }

    public static void insert(String tableName, JSONObject obj, SQLiteDatabase db) {
        if (!obj.has(JSON_ID))
            throw new RuntimeException("Json object do not contains id");

        ContentValues values = new ContentValues();
        try {
            if (mCash.containsKey(tableName))
              mCash.get(tableName).put(obj.getString("id"), obj);
            values.put(JSON_ID, obj.get(JSON_ID).toString());
            values.put(JSON_FIELD, obj.toString());
        } catch (JSONException e) {
            System.out.println("Some value in JSON is empty");
        }

        System.out.println("DBhelper data insert: " + obj);
        db.insert(tableName, null, values);
    }

    public static HashMap<String, JSONObject> getTable(String tableName) {
        if (mCash.containsKey(tableName))
            return mCash.get(tableName);

        init();
        return mCash.get(tableName);
    }

    public static JSONObject get(String table, String id) {
        if (hasInCash(table, id)) {
            JSONObject json = mCash.get(table).get(id);
            return json;
        }

        SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();

        Cursor c = db.query(table, new String[]{JSON_FIELD},
                JSON_ID + " =  ?",
                new String[]{id},
                null, null, null);

        if (c.getCount() == 0) {
            System.out.println("Selection from database for table " + table + " with id " + id + " FAILED");
            return null;
        }

        JSONObject json = null;
        c.moveToFirst();
        try {
            json = new JSONObject(
                    new String(c.getBlob(c.getColumnIndex(JSON_FIELD))));
        } catch (JSONException e) {
            System.out.println("Json from table is broken");
        }

        return json;
    }

    public static boolean has(String table, String id) {
        if (hasInCash(table, id))
            return true;

        SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();
        Cursor cursor = db.query(table,
                new String[] {JSON_ID},
                "NAME = ?",
                new String[] {id},
                null, null, null);

        return cursor.isNull(0);
    }

    public static boolean hasInCash(String table, String id) {
        return mCash.containsKey(table);
    }

    public static void remove(String table, String id) {
        SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();
        db.delete(table, JSON_ID + " = " + id, null);
        mCash.get(table).remove(id);
    }

    private static void init() {
        if (!mCash.containsKey(BOOKINGS_TABLE)) {
            mCash.put(BOOKINGS_TABLE, new HashMap<>());
            String query = "SELECT bookings_table.json FROM " + BOOKINGS_TABLE + ";";
            SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();
            Cursor c = db.rawQuery(query, null);
            c.moveToFirst();
            JSONObject json;

            while (!c.isAfterLast()) {
                byte[] jsonBytes = c.getBlob(c.getColumnIndex(JSON_FIELD));
                c.moveToNext();
                try {
                    json = new JSONObject(new String(jsonBytes));
                    Log.debug(getInstance(MainActivity.getContext()), "Booking getted: " + json);
                    mCash.get(BOOKINGS_TABLE).put(json.getString("id"), json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if (!mCash.containsKey(HOUSE_TABLE)) {
            mCash.put(HOUSE_TABLE, new HashMap<>());
            String query = "SELECT house_table.json FROM " + HOUSE_TABLE + ";";
            SQLiteDatabase db = getInstance(MainActivity.getContext()).getWritableDatabase();
            Cursor c = db.rawQuery(query, null);
            c.moveToFirst();
            JSONObject json;

            while (!c.isAfterLast()) {
                byte[] jsonBytes = c.getBlob(c.getColumnIndex(JSON_FIELD));
                c.moveToNext();
                try {
                    json = new JSONObject(new String(jsonBytes));
                    Log.debug(getInstance(MainActivity.getContext()), "House downloaded : " + json);
                    mCash.get(HOUSE_TABLE).put(json.getString("id"), json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
        non-static part
    */

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_DATABASE);
        mCash = new HashMap<>();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(queryCreateTable(BOOKINGS_TABLE));
        db.execSQL(queryCreateTable(TABLE_SERVICE));
        db.execSQL(queryCreateTable(HOUSE_TABLE));
        JSONObject json = new JSONObject();
        try {
            json.put(JSON_ID, DB_ID_COUNTER);
            json.put(DB_ID_COUNTER, "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        insert(TABLE_SERVICE, json, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static String queryCreateTable(String tableName) {
        return "CREATE TABLE IF NOT EXISTS " + tableName
                + " ( " + JSON_ID + " VARCHAR(100) NOT NULL, json BLOB);";
    }

    public static final String DATABASE_NAME = "booker_database";
    public static final String BOOKINGS_TABLE = "bookings_table";
    public static final String HOUSE_TABLE = "house_table";
    public static final String TABLE_SERVICE = "extra_table";
    public static final String JSON_ID = "id";
    public static final String JSON_FIELD = "json";
    public static final String DB_ID_COUNTER = "id_counter";
    public static final int VERSION_DATABASE = 1;

    private static HashMap<String, HashMap<String, JSONObject>> mCash; // table-name -> list(id -> booking)
    private static DBHelper mInstance = null;
    private static long idCounter = -1; // initialized by value from database !!!
}
